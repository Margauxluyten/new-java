package com.vdab;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        Teacher lizzy = new Teacher(1,"Lizzy",500);
        Teacher melissa = new Teacher(2,"Melissa",700);
        Teacher vanderhorn = new Teacher(3,"Vanderhorn",600);

        List<Teacher> teacherList = new ArrayList<>();
        teacherList.add(lizzy);
        teacherList.add(melissa);
        teacherList.add(vanderhorn);

        Student tamara = new Student(1,"Tamara",4);
        Student margaux = new Student(2,"Margaux",12);
        Student rabbi = new Student(3,"rabbi",5);
        List<Student> studentList= new ArrayList<>();
        studentList.add(tamara);
        studentList.add(margaux);
        studentList.add(rabbi);

        School ghs = new School(teacherList,studentList);

        tamara.payFees(5000);

        System.out.println("GHS School Total money earned is "+ ghs.getTotalMoneyEarned());

        margaux.payFees(6000);

        System.out.println("GHS School Total money earned is "+ ghs.getTotalMoneyEarned());

        System.out.println("-----------making ghs pay salary--------------");

        lizzy.receiveSalary(lizzy.getSalary());
        System.out.println("ghs has paid the salary to " + lizzy.getName()+ " and now has " + ghs.getTotalMoneyEarned());

        vanderhorn.receiveSalary(vanderhorn.getSalary());

        System.out.println("ghs has paid the salary to " + vanderhorn.getName()+ " and now has " + ghs.getTotalMoneyEarned());

        System.out.println(margaux);



    }
}
