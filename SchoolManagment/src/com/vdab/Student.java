package com.vdab;

//this class is responsible for keeping the track of the student fees , name , grade and fees

public class Student {

    private int id;
    private String name;
    private int grade;
    private int feesPaid;
    private int feesTotal;

    public Student(int id, String name, int grade) {
        this.id = id;
        this.name = name;
        this.grade = grade;
        this.feesPaid=0;
        this.feesTotal=30000;
    }
    //used top update student grade
    public void setGrade(int grade){
        this.grade = grade;

    }
    // add the fees to the fees paid
    public void payFees(int fees){
        feesPaid+=fees;
        School.updateTotalMoneyEarned(feesPaid);


    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getGrade() {
        return grade;
    }

    public int getFeesPaid() {
        return feesPaid;
    }

    public int getFeesTotal() {
        return feesTotal;
    }

    public int getRemainingFees(){
        return feesTotal-feesPaid;
    }

    @Override
    public String toString() {
        return "student name : "+name+" tot fees payed so far " + feesPaid;
    }
}
