package com.vdab;



public class Teacher {

    private int id;
    private String name;
    private int salary;
    private int salaryearned;

    public Teacher(int id, String name, int salary) {
        this.id = id;
        this.name = name;
        this.salary = salary;
        this.salaryearned = 0;

    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public void receiveSalary(int salary){

        salaryearned+=salary;
        School.updateTotalMoneySpent(salary);



    }
}
