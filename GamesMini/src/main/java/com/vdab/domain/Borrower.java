package com.vdab.domain;

import lombok.Data;

@Data

public class Borrower extends BaseEntity{

    private String borrowerName;
    private String street;
    private String houseNumber;
    private String busNumber;
    private String postCode;
    private String city;
    private String telephone;
    private String email;
}
