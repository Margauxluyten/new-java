package com.vdab.domain;

import lombok.Data;

import java.time.LocalDate;
@Data

public class Borrow extends BaseEntity{
    private Game game;
    private Borrower borrower;
    private LocalDate borrowDate;
    private LocalDate returnDate;


}
