package com.vdab.services;

import com.vdab.domain.Game;
import com.vdab.repositories.GameRepository;

public class GameService implements GameServiceI {
    GameRepository gameRepository = new GameRepository();


    @Override
    public Game findById(int id) {
        return gameRepository.findById(id);
    }
}
