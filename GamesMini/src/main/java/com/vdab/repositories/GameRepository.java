package com.vdab.repositories;

import com.vdab.domain.Category;
import com.vdab.domain.Difficulty;
import com.vdab.domain.Game;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class GameRepository {

    public Game findById(int id) {
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/games", "root", "P@ssw0rd")) {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from game as g inner join category as c on g.category_id = c.id inner join difficulty as d on g.difficulty_id = d.id where g.id = ? ");
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();
            resultSet.next();
            Game.builder()
                    .id(resultSet.getInt("g.id"))
                    .gameName(resultSet.getString("game_name"))
                    .editor(resultSet.getString("editor"))
                    .author(resultSet.getString("author"))
                    .yearEdition(resultSet.getInt("year_edition"))
                    .age(resultSet.getString("age"))
                    .minPlayers(resultSet.getInt("min_players"))
                    .maxPlayers(resultSet.getInt("max_players"))
                    .category(new Category(resultSet.getInt("id"),resultSet.getString("category_name")))
                    .playDuration(resultSet.getString("play_duration"))
                    .difficulty(new Difficulty(resultSet.getInt("id"),resultSet.getString("difficulty_name")))
                    .price(resultSet.getDouble("price"))
                    .image(resultSet.getString("image"))
                    .build();


            return Game.builder().gameName(resultSet.getString("game_name")).build();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
