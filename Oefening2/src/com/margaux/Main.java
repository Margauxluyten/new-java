package com.margaux;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    static ArrayList<Human> humans = new ArrayList<>();
    static ArrayList<Animal> animals = new ArrayList<>();
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Welcome to earth !");
        showInitialOptions();



    }
    // ok this is a comment


    private static void showInitialOptions() {
        System.out.println("please select an option : "
                + "\n\t1. manage humans"
                + "\n\t2. manage animals"
                + "\n\t3. quit");

        int choice = scanner.nextInt();

        switch (choice) {
            case 1:
                manageHumans();
                break;
            case 2:
                manageAnimals();
                break;
            default:
                break;

        }
    }

    private static void manageAnimals() {

        System.out.println("please select an option : "
                + "\n\t1. add animals"
                + "\n\t2. search animals"
                + "\n\t3. show all animals"
                + "\n\t4. go back");

        int choice = scanner.nextInt();

        switch (choice) {
            case 1:
                addAnimal();
                break;
            case 2:
                searchAnimal();
                break;
            case 3:
                showAllAnimals();
                break;
            case 4 :
                showInitialOptions();
            default:
                System.out.println("not a valid input ");
                manageHumans();

        }

    }

    private static void showAllAnimals() {
        for (Animal a : animals){
            a.getDetails();
            System.out.println("************************");

        }showInitialOptions();
    }

    private static void searchAnimal() {
        System.out.println("Type in the name of the animal you want to search for ? ");
        String name = scanner.next();
        boolean doesExist = false ;
        for (Animal a :animals){
            if(a.getName().equals(name)){
                doesExist = true;
                a.getDetails();
                System.out.println("*******************************");
            }if(!doesExist){
                System.out.println("There is no such animal with the name :"+name);
                showInitialOptions();
            }
            System.out.println("Redirecting to main menu ..");
            showInitialOptions();
        }
    }

    private static void manageHumans() {

        System.out.println("please select an option : "
                + "\n\t1. add a human"
                + "\n\t2. search for human"
                + "\n\t3. show all humans"
                + "\n\t4. go back");

        int choice = scanner.nextInt();

        switch (choice) {
            case 1:
                addHuman();
                break;
            case 2:
                searchForHuman();
                break;
            case 3 : showAllHumans();
            break;
            case 4:
                showInitialOptions();
                break;
            default:
                System.out.println("not a valid input");
                manageHumans();

        }



    }

    private static void showAllHumans() {
        for (Human h : humans){
            h.getDetails();
            System.out.println("************************");

        }showInitialOptions();
    }

    private static void searchForHuman() {
        System.out.println("Type in the name of the person you want to search for ? ");
        String name = scanner.next();
        boolean doesExist = false ;
        for (Human h :humans){
            if(h.getName().equals(name)){
                doesExist = true;
                h.getDetails();
                System.out.println("*******************************");
            }if(!doesExist){
                System.out.println("There is no such human with the name :"+name);
                showInitialOptions();
            }
            System.out.println("Redirecting to main menu ..");
            showInitialOptions();
        }
    }

    private static void addHuman() {
        System.out.println("what is the name of the Human? ");
        String name = scanner.next();
        System.out.println("what is the age of the Human? ");
        int age = scanner.nextInt();
        if(name.equals("") || age <0 ||age > 120){
            System.out.println("please enter valid information");
            addHuman();
        }else{
            System.out.println("human:" + name + " has been added");
            Human human = new Human(name,age);
            humans.add(human);
            showInitialOptions();
        }

    }

    private static void addAnimal() {
        System.out.println("what is the name of the animal? ");
        String name = scanner.next();
        System.out.println("what is the age of the animal? ");
        int age = scanner.nextInt();
        if(name.equals("") || age <0 ||age > 120){
            System.out.println("please enter valid information");
            addAnimal();
        }else{
            System.out.println("animal:" + name + " has been added");
            Animal animal = new Animal(name,age);
            animals.add(animal);
            showInitialOptions();
        }
        

        }
    }

