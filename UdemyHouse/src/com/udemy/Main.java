package com.udemy;

class Car{
    private boolean engine;
    private int cylinders;
    private String name;
    private int wheels;

    public Car(int cylinders, String name) {
        this.cylinders = cylinders;
        this.name = name;
        this.wheels =4;
        this.engine = true;
    }

    public int getCylinders() {
        return cylinders;
    }

    public String getName() {
        return name;
    }
    public String startEngine(){
        return "Car -> StartEngine()";
    }
    public String accelarate(){
        return "Car-> eccelarate";
    }
    public String brake(){
        return "Car->brake";
    }
}
class Mitsubishi extends Car{

    public Mitsubishi(int cylinders, String name) {
        super(cylinders, name);
    }

    @Override
    public String startEngine() {
        return "Mitsubishi-> startEngine()";
    }

    @Override
    public String accelarate() {
        return "Mitsubishi-> accelarate()";
    }

    @Override
    public String brake() {
        return "Mitsubishi-> brake ()";
    }
}

public class Main {
    public static void main(String[] args) {

      /*  Printer printer = new Printer(50,false);
        System.out.println("initial page count = "+ printer.getPagesPrined());
        int pagesPrinted = printer.printPages(4);
        System.out.println("pages printed was " + pagesPrinted+ " new total print count for printer = " + printer.getPagesPrined());
        pagesPrinted = printer.printPages(2);
        System.out.println("pages printed was " + pagesPrinted+ " new total print count for printer = " + printer.getPagesPrined());

       */

        Car car = new Car(8, "Base car");
        System.out.println(car.accelarate());
        System.out.println(car.brake());
        System.out.println(car.startEngine());

        Mitsubishi mitsubishi = new Mitsubishi(6, "outlander vrw");
        System.out.println(mitsubishi.accelarate());
        System.out.println(mitsubishi.brake());
        System.out.println(mitsubishi.startEngine());

        Ford ford = new Ford(8,"Mustang");
        System.out.println(ford.accelarate());
        System.out.println(ford.brake());
        System.out.println(ford.startEngine());



    }

    static class Audi extends Car{

        public Audi(int cylinders, String name) {
            super(cylinders, name);
        }

        @Override
        public String startEngine() {
            return "Audi-> startEngine()";
        }

        @Override
        public String accelarate() {
            return "Audi-> accelarate()";
        }

        @Override
        public String brake() {
            return "Audi-> brake ()";
        }
    }

    static class Ford extends Car{

        public Ford(int cylinders, String name) {
            super(cylinders, name);
        }

        @Override
        public String startEngine() {
            return "Ford-> startEngine()";
        }

        @Override
        public String accelarate() {
            return "Ford-> accelarate()";
        }

        @Override
        public String brake() {
            return "Ford-> brake ()";
        }
    }
}
