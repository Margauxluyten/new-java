package com.udemy.academy;

public class VipCustomer {
    private String name;
    private double creditLimit;
    private String emailAdress;

    public VipCustomer() {
        this("default name",5000, "default emailAdress");
        System.out.println("empty constructor");
    }

    public VipCustomer(String name, double creditLimit) {
       this(name,creditLimit,"default unknown ");
    }

    public VipCustomer(String name, double creditLimit, String emailAdress) {
        this.name = name;
        this.creditLimit = creditLimit;
        this.emailAdress = emailAdress;
    }


    public String getName() {
        return name;
    }

    public double getCreditLimit() {
        return creditLimit;
    }

    public String getEmailAdress() {
        return emailAdress;
    }


}
