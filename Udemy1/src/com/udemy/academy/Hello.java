package com.udemy.academy;

import java.util.Scanner;

public class Hello {
    public static void main(String[] args) {

        Dimensions dimensions = new Dimensions(20,20,5);
        Case theCase = new Case("220B","dell","240",dimensions);
        Monitor theMonitor = new Monitor("27inch Beast", "acer",27, new Resolution(2540,1140));
        Motherboard theMotherboard = new Motherboard("BJ-200","asus",4,6,"v2.44");

        PC thePC = new PC(theCase,theMonitor,theMotherboard);
        thePC.powerUp();

    }
}










