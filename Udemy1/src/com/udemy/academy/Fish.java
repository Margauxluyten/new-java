package com.udemy.academy;

public class Fish extends Animalnheritance {
    private int gills;
    private int eyes;
    private int fins;

    public Fish(String name, int size, int weight, int gills, int eyes, int fins) {
        super(name, 1, 1, size, weight);
        this.gills = gills;
        this.eyes = eyes;
        this.fins = fins;
    }

    private void rest(){

    }

    private void moveMucles(){

    }

    private void moveBackFin(){

    }

    private void swimm(int speed){
        moveMucles();
        moveBackFin();
        super.move(speed);

    }
}
