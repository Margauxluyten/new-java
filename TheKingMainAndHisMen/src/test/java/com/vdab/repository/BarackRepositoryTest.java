package com.vdab.repository;

import com.vdab.domain.Soldier;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class BarackRepositoryTest {

    private BarackRepository barackRepository =new BarackRepository();

    private Soldier soldier;

    @AfterEach
    public void cleanUpDataBase(){
        barackRepository.deleteById(soldier.getId());
    }

    @Test
    void save(){
        this.soldier = new Soldier("Jeff");
        barackRepository.save(soldier);
        Soldier soldierFromDb = barackRepository.findById(soldier.getId());
        Assertions.assertEquals("Jeff",soldierFromDb.getName());

    }

}