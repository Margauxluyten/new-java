package com.vdab.services;


import com.vdab.domain.Soldier;
import com.vdab.repository.BarackRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class SoldiersServiceTest {
    @InjectMocks
    private SoldierCommands soldierService= new SoldierServicelmpl();
    @Mock
    private BarackRepository barackRepository;

    @Test
    @DisplayName("Be sure that sent to barrack calls repo one time")
    public void assertThatSetToBarracks(){
        Soldier soldier = new Soldier("Max");
        soldierService.sendToBarracks(soldier);
        Mockito.verify(barackRepository).save(soldier);

    }

}
