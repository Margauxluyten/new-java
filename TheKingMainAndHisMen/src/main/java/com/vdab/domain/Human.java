package com.vdab.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder

public class Human {
    private int legs;
    private int arms;
    private String name;
    private int head;
    private int id;


    Human(String name) {
        this.legs = 2;
        this.arms = 2;
        this.name = name;
        this.head = 1;
    }


}
