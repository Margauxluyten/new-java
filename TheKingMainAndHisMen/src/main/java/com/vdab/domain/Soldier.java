package com.vdab.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
@ToString

public class Soldier extends Human{
    public static final String MY_KING= "King main";
    private int defence;
    private int attack;
    private String weapon;
    private int recoverySpeed;
    private int health;

    public Soldier(String name){

        super(name);
        defence=50;
        attack=60;
        weapon="SWORD";
        recoverySpeed=20;
        health=100;
    }

}
