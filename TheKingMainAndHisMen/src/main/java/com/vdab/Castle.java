package com.vdab;

import com.vdab.domain.Soldier;
import com.vdab.services.SoldierCommands;
import com.vdab.services.SoldierServicelmpl;
import org.slf4j.ILoggerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Scanner;

public class Castle {

    private static SoldierCommands soldierService = new SoldierServicelmpl() ;
    private static Logger logger = LoggerFactory.getLogger(Castle.class);


    public static void main(String[] args) {

        logger.info("Welcome to my kingdom !");
        logger.debug("Debug logger");
        logger.error("error logger");
        logger.trace("trace logger");
        logger.warn("warn logger ");



        System.out.println("Who is your king ?");
        System.out.println("Our king is : " + Soldier.MY_KING);

       Soldier newSoldier = createSoldier();
       soldierService.sendToBarracks(newSoldier);

    }
    private static Soldier createSoldier(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("What are the names of the soldiers?");
        Soldier mySoldier = new Soldier(scanner.next());
        System.out.println(mySoldier.MY_KING);
        System.out.println("My name is " + mySoldier.getName());
        System.out.println("How healthy are you ?");
        System.out.println("My health is " +  mySoldier.getHealth());
        return mySoldier;


    }
}
