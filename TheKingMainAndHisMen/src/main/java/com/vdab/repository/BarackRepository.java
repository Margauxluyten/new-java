package com.vdab.repository;

import com.vdab.domain.Soldier;

import java.sql.*;

public class BarackRepository {
    public void save(Soldier soldier) {

        try(Connection connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/kingdom","root","P@ssw0rd")){
            PreparedStatement statement= connection.prepareStatement("insert barack(name) values (?)",Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, soldier.getName());
            statement.execute();

            ResultSet generatedKeys = statement.getGeneratedKeys();
            generatedKeys.next();
            soldier.setId(generatedKeys.getInt(1));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    public Soldier findById(int id){

        try(Connection connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/kingdom","root","P@ssw0rd")){
            PreparedStatement statement= connection.prepareStatement("select * from barack where id = ?");
            statement.setInt(1,id);
            statement.execute();
            ResultSet resultSet= statement.getResultSet();
            resultSet.next();

            Soldier soldier = new Soldier(resultSet.getString("name"));
            return soldier;


        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return null;
    }

    public void deleteById(int id) {

        try(Connection connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/kingdom","root","P@ssw0rd")){
            PreparedStatement statement= connection.prepareStatement("delete from barack where id =?");
            statement.setInt(1,id);
            statement.execute();


        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
