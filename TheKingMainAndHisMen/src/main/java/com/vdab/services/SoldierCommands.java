package com.vdab.services;

import com.vdab.domain.Soldier;

import java.util.List;

public interface SoldierCommands {

    public void sendToBarracks(Soldier soldier);

    List<Soldier> findAll();
}
