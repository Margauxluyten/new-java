package com.vdab.commandpattern;

public interface Command {

    void execute();
}
