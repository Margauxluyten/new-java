package com.vdab.domain;

import lombok.Data;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Data

public class Product extends BaseEntity {

    private String productName;
    private  Category category;
    private String description;
    private Double wholeSale;
    private int quantity;
    private double retailPrice;
    private Store store;
}
