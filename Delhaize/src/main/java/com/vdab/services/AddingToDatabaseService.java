package com.vdab.services;

import com.vdab.domain.Category;
import com.vdab.domain.Product;
import com.vdab.repositories.AddingToDatabaseRepository;


public class AddingToDatabaseService {

    private AddingToDatabaseRepository addingToDatabaseRepository= new AddingToDatabaseRepository();

    public static void addProductToDB(Product product) {
        AddingToDatabaseRepository.addProductToDB(product);
    }

    public Category getCategoryById(int id) {

        return AddingToDatabaseRepository.getCategoryById(id);
    }
}

