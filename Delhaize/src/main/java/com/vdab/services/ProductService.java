package com.vdab.services;

import com.vdab.domain.Product;
import com.vdab.repositories.ProductRepository;

import java.util.List;

public class ProductService {

    ProductRepository productRepository = new ProductRepository();

    public List<Product> listOfProducts() {
        return productRepository.listOfProducts();
    }



    public List<Product> listOfSpecificProducts(int storeId) {
        return productRepository.listOfSpecificProducts(storeId);
    }
}
