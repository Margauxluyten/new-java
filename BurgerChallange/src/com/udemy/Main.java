package com.udemy;

public class Main {
    public static void main(String[] args) {

        BaseBurger baseBurger =new BaseBurger("Basic","minced meat",3.50,"white");
        double price = baseBurger.itemizeHamburger();
        baseBurger.addHamburgerAddition1("Tomato",0.25);
        baseBurger.addHamburgerAddition2("Lettuce",0.75);
        baseBurger.addHamburgerAddition3("Cheese",1);
        System.out.println("Total burger price = "+baseBurger.itemizeHamburger());

        HealthyBurger healthyBurger = new HealthyBurger("vegan",5.67);
        healthyBurger.addHamburgerAddition1("egg",5.43);
        healthyBurger.addHealthAddition1("lentils",2.50);
        System.out.println("total healthy burger price is "+healthyBurger.itemizeHamburger());

        DeluxeBurger deluxeBurger= new DeluxeBurger();
        deluxeBurger.addHamburgerAddition3("should not do this ",50.00);
        deluxeBurger.itemizeHamburger();
    }
}
