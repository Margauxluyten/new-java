package com.udemy;

public class DeluxeBurger extends BaseBurger{
    public DeluxeBurger() {
        super("deluxe", "sausage & bacon", 14.50, "white");
        super.addHamburgerAddition1("chips",2.75);
        super.addHamburgerAddition2("drink",1.80);
    }

    @Override
    public void addHamburgerAddition1(String name, double price) {
        System.out.println("Can not add additional items to the burgers");
    }

    @Override
    public void addHamburgerAddition2(String name, double price) {
        System.out.println("Can not add additional items to the burgers");
    }

    @Override
    public void addHamburgerAddition3(String name, double price) {
        System.out.println("Can not add additional items to the burgers");
    }

    @Override
    public void addHamburgerAddition4(String name, double price) {
        System.out.println("Can not add additional items to the burgers");
    }
}
